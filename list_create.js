function list_create() {
    //マスタやスタッフリストは　性別は男　女　になっているので　男性　女性　へ直す

    function _get_people() {
        // ->array
        var url =
            "https://docs.google.com/spreadsheets/d/1BtYYlEtJeNa0qddS4l_gyf1QylEgDNQyCuFLyEOV7Y0/edit#gid=0";
        var sheet_name = "患者マスタ";
        var Master = new Appdate.Master(url, sheet_name, 1, 1);

        //患者マスタからの情報取得
        var people = [...Master.values_obj.keys()]
            .filter((d) => Master.values_obj[d]["現状"] == "透析中")
            .map((d) => [
                Master.values_obj[d]["患者氏名"],
                "",
                Master.values_obj[d]["フリガナ苗字"],
                Master.values_obj[d]["フリガナ名前"],
                Master.values_obj[d]["性別"] + "性",
                Moment.moment(Master.values_obj[d]["生年月日"]).format("YYYY/MM/DD"),
                Master.values_obj[d]["入居施設"],
                "", //結果
                Master.values_obj[d]["Quolis_id"],
            ]);
        var url =
            "https://docs.google.com/spreadsheets/d/10LUfCkmd_RUhbU7DhkAiSxF6RSZEpX8Lz0RUIUwZbyw/edit#gid=0";
        var sheet_name = "シート1";
        var Staff = new Appdate.Master(url, sheet_name, 1, 1);

        [...Staff.values_obj.keys()].map(d => (people.push([
            Staff.values_obj[d]["氏"],
            Staff.values_obj[d]["名"],
            Staff.values_obj[d]["フリガナ苗字"],
            Staff.values_obj[d]["フリガナ名前"],
            Staff.values_obj[d]["性別"] + "性",
            Moment.moment(Staff.values_obj[d]["生年月日"]).format("YYYY/MM/DD"),
            "透析CL",
            "", //結果
            Staff.values_obj[d]["quolis"],
        ])))

        return people;
    }

    //var people = _get_people();

    function _get_sample() {
        var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
        var sheet = spreadsheet.getActiveSheet();
        var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
        var sheet = spreadsheet.getActiveSheet();
        var sheet_name = sheet.getName()
        var url = spreadsheet.getUrl()
        var Sample = new Appdate.Master(url, sheet_name, 2, 2);
        return Sample
    }

    var Sample = _get_sample()
    var id = [...Sample.values_obj.keys()].filter(d => Sample.values_obj[d]["id"] !== "").map(d => Sample.values_obj[d]["id"])

    var peoples = people.filter(p => id.includes(p[8]))

    Sample.sheet.getRange(3, 2, peoples.length, 9).setValues(peoples)
}
